"""
Программа, которая:
    - получает информацию о всех астронафтах в космосе
    - записывает эту информацию в people.csv
    - сортирует космонафтов по имени корабля и дополнительно создает csv файлы с астронафтами в отдельных кораблях
"""

import requests
import csv
from collections import defaultdict


astronauts_list = requests.get('http://api.open-notify.org/astros.json').json()['people']
headers_for_all_astronauts = ['id', 'first_name', 'last_name', 'craft']
headers_for_crafts = ['id', 'first_name', 'last_name']

all_astronauts_in_space = []
dict_by_crafts = defaultdict(list)

for i in range(len(astronauts_list)):
    full_name = astronauts_list[i]['name'].split(' ')

    astro_first_name = full_name.pop(0)
    astro_last_name = ' '.join(full_name)
    astro_craft = astronauts_list[i]['craft']

    # Наполняется список всех астронафтов в космосе
    all_astronauts_in_space.append({
        'id': i,
        'first_name': astro_first_name,
        'last_name': astro_last_name,
        'craft': astro_craft
    })

    # Наполняется словарь где ключ - название корабля, а значение - список его экипажа
    dict_by_crafts[astro_craft].append({
        'id': i,
        'first_name': astro_first_name,
        'last_name': astro_last_name
    })


def write_to_csv(file_name, headers, data):
    """
    Функция пишет в csv файл заголовок и всю информацию, которую ей передают

    :param file_name: имя csv файла, в который будет записываться информация
    :param headers: заголовки для таблицы
    :param data: информация которая будет передаваться
    """
    with open(f'files/{file_name}.csv', 'w+') as file:
        writer = csv.DictWriter(file, headers)
        writer.writeheader()
        writer.writerows(data)


# Записывается список всех астронафтов в космосе в 'people.csv'
write_to_csv('people', headers_for_all_astronauts, all_astronauts_in_space)

# Записывается список астронафтов в отдельные csv файлы по каждому кораблю
for craft in dict_by_crafts:
    write_to_csv(craft, headers_for_crafts, dict_by_crafts[craft])
